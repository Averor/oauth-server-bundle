<?php declare(strict_types=1);

namespace Averor\OAuthServerBundle\DependencyInjection;

use Averor\OAuthServerBundle\Repository\AccessTokenRepository;
use Averor\OAuthServerBundle\Repository\AuthCodeRepository;
use Averor\OAuthServerBundle\Repository\ClientRepository;
use Averor\OAuthServerBundle\Repository\DatabaseFacade;
use Averor\OAuthServerBundle\Repository\Mongo\MongoFacade;
use Averor\OAuthServerBundle\Repository\RefreshTokenRepository;
use League\OAuth2\Server\AuthorizationServer;
use League\OAuth2\Server\Grant\PasswordGrant;
use League\OAuth2\Server\Grant\RefreshTokenGrant;
use League\OAuth2\Server\Repositories\UserRepositoryInterface;
use League\OAuth2\Server\ResourceServer;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\HttpKernel\DependencyInjection\ConfigurableExtension;

/**
 * Class BundleExtension
 *
 * @package Averor\OAuthServerBundle\DependencyInjection
 * @author Averor <averor.dev@gmail.com>
 */
class BundleExtension extends ConfigurableExtension
{
    public function getAlias()
    {
        return 'oauth2server';
    }

    /**
     * @param array $config
     * @param ContainerBuilder $container
     * @return Configuration|null|object|\Symfony\Component\Config\Definition\ConfigurationInterface
     */
    public function getConfiguration(array $config, ContainerBuilder $container)
    {
        return new Configuration();
    }

    /**
     * Configures the passed container according to the merged configuration.
     *
     * @param array $mergedConfig
     * @param ContainerBuilder $container
     * @throws \Exception
     */
    protected function loadInternal(array $mergedConfig, ContainerBuilder $container)
    {
        $loader = new YamlFileLoader(
            $container,
            new FileLocator(dirname(__DIR__).'/Resources/config')
        );

        $loader->load('services.yaml');

        $serverDefinition = $container->getDefinition(AuthorizationServer::class)
            ->setArgument('$privateKey', $mergedConfig['private_key_path'])
            ->setArgument('$encryptionKey', $mergedConfig['encryption_key']);

        $container->getDefinition(ResourceServer::class)
            ->setArgument('$publicKey', $mergedConfig['public_key_path']);

        $container->getDefinition('AccessTokenTTL')
            ->setArgument('$time', sprintf("%d seconds", $mergedConfig['access_token_ttl']));

        $container->getDefinition('RefreshTokenTTL')
            ->setArgument('$time', sprintf("%d days", $mergedConfig['refresh_token_ttl']));

        $container->setAlias(
            UserRepositoryInterface::class,
            $mergedConfig['repository']['userRepositoryServiceId']
        );

        $container->getDefinition(AccessTokenRepository::class)
            ->setArgument('$accessTokenRepoCollectionName', $mergedConfig['repository']['accessTokenCollection']);

        $container->getDefinition(AuthCodeRepository::class)
            ->setArgument('$accessTokenRepoCollectionName', $mergedConfig['repository']['authCodeCollection']);

        $container->getDefinition(ClientRepository::class)
            ->setArgument('$accessTokenRepoCollectionName', $mergedConfig['repository']['clientCollection']);

        $container->getDefinition(RefreshTokenRepository::class)
            ->setArgument('$accessTokenRepoCollectionName', $mergedConfig['repository']['refreshTokenCollection']);

        if (in_array('password', $mergedConfig['grant_types'])) {
            $container->getDefinition(PasswordGrant::class)
                ->addMethodCall(
                    'setRefreshTokenTTL',
                    [$container->getDefinition('RefreshTokenTTL')]
                );

            $serverDefinition->addMethodCall(
                'enableGrantType',
                [
                    $container->getDefinition(PasswordGrant::class),
                    $container->getDefinition('AccessTokenTTL')
                ]
            );
        } else {
            $container->removeDefinition(PasswordGrant::class);
        }

        switch ($mergedConfig['repository']['driver']) {
            case 'mongo':
                $container->setAlias(
                    DatabaseFacade::class,
                    MongoFacade::class
                );
                break;
            default:
                throw new \InvalidArgumentException('Only mongo currently supported');
        }

        if (in_array('refresh', $mergedConfig['grant_types'])) {
            $serverDefinition->addMethodCall(
                'enableGrantType',
                [
                    $container->getDefinition(RefreshTokenGrant::class),
                    $container->getDefinition('AccessTokenTTL')
                ]
            );
        } else {
            $container->removeDefinition(RefreshTokenGrant::class);
        }


    }
}
