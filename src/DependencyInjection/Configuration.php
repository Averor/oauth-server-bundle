<?php declare(strict_types=1);

namespace Averor\OAuthServerBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * Class Configuration
 *
 * @package Averor\OAuthServerBundle\DependencyInjection
 * @author Averor <averor.dev@gmail.com>
 */
class Configuration implements ConfigurationInterface
{
    /**
     * Generates the configuration tree builder.
     *
     * @return \Symfony\Component\Config\Definition\Builder\TreeBuilder The tree builder
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();

        $rootNode = $treeBuilder->root('oauth2server');

        $rootNode
            ->children()

                ->arrayNode('grant_types')
                    ->isRequired()
                    ->requiresAtLeastOneElement()
                    ->enumPrototype()
                        ->isRequired()
                        ->values(['client_credentials', 'password', 'auth_code', 'implicit', 'refresh'])
                    ->end()
                ->end()

                ->scalarNode('private_key_path')->isRequired()->end()
                ->scalarNode('encryption_key')->isRequired()->end()
                ->scalarNode('public_key_path')->isRequired()->end()

                ->integerNode('access_token_ttl')->min(10)->max(86400)->defaultValue(7200)->end()
                ->integerNode('refresh_token_ttl')->min(1)->max(30)->defaultValue(30)->end()

                ->arrayNode('repository')
                    ->children()
                        ->enumNode('driver')->isRequired()->values(['mongo'])->end()

                        ->scalarNode('service')->isRequired()->end()

                        ->scalarNode('accessTokenCollection')->isRequired()->end()
                        ->scalarNode('authCodeCollection')->isRequired()->end()
                        ->scalarNode('clientCollection')->isRequired()->end()
                        ->scalarNode('refreshTokenCollection')->isRequired()->end()

                        ->scalarNode('userRepositoryServiceId')->isRequired()->end()

                    ->end()
                ->end()

            ->end();


        return $treeBuilder;
    }
}
