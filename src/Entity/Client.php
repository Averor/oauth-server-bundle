<?php declare(strict_types=1);

namespace Averor\OAuthServerBundle\Entity;

use League\OAuth2\Server\Entities\ClientEntityInterface;
use League\OAuth2\Server\Entities\Traits\ClientTrait;
use League\OAuth2\Server\Entities\Traits\EntityTrait;

/**
 * Class Client
 *
 * @package Averor\OAuthServerBundle\Entity
 * @author Averor <averor.dev@gmail.com>
 */
class Client implements ClientEntityInterface
{
    use ClientTrait, EntityTrait;

    /**
     * Create a new client instance.
     *
     * @param  string  $identifier
     * @param  string  $name
     * @param  string  $redirectUri
     * @return void
     */
    public function __construct($identifier, $name, $redirectUri)
    {
        $this->setIdentifier((string) $identifier);
        $this->name = $name;
        $this->redirectUri = $redirectUri
            ? explode(',', $redirectUri)
            : null;
    }
}
