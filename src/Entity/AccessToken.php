<?php declare(strict_types=1);

namespace Averor\OAuthServerBundle\Entity;

use League\OAuth2\Server\Entities\ClientEntityInterface;
use League\OAuth2\Server\Entities\Traits\EntityTrait;
use League\OAuth2\Server\Entities\Traits\AccessTokenTrait;
use League\OAuth2\Server\Entities\Traits\TokenEntityTrait;
use League\OAuth2\Server\Entities\AccessTokenEntityInterface;

/**
 * Class AccessToken
 *
 * @package Averor\OAuthServerBundle\Entity
 * @author Averor <averor.dev@gmail.com>
 */
class AccessToken implements AccessTokenEntityInterface
{
    use AccessTokenTrait, EntityTrait, TokenEntityTrait;

    /**
     * Create a new token instance.
     *
     * @param ClientEntityInterface $clientEntity
     * @param  string|int $userIdentifier
     * @param  array $scopes
     */
    public function __construct(ClientEntityInterface $clientEntity, $userIdentifier, array $scopes = [])
    {
        $this->setClient($clientEntity);
        $this->setUserIdentifier($userIdentifier);
        foreach ($scopes as $scope) {
            $this->addScope($scope);
        }
    }
}
