<?php declare(strict_types=1);

namespace Averor\OAuthServerBundle\Entity;

use League\OAuth2\Server\Entities\Traits\EntityTrait;
use League\OAuth2\Server\Entities\Traits\RefreshTokenTrait;
use League\OAuth2\Server\Entities\RefreshTokenEntityInterface;

/**
 * Class RefreshToken
 *
 * @package Averor\OAuthServerBundle\Entity
 * @author Averor <averor.dev@gmail.com>
 */
class RefreshToken implements RefreshTokenEntityInterface
{
    use EntityTrait, RefreshTokenTrait;
}
