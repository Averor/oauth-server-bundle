<?php declare(strict_types=1);

namespace Averor\OAuthServerBundle\Entity;

use League\OAuth2\Server\Entities\Traits\EntityTrait;
use League\OAuth2\Server\Entities\UserEntityInterface;

/**
 * Class User
 *
 * @package Averor\OAuthServerBundle\Entity
 * @author Averor <averor.dev@gmail.com>
 */
class User implements UserEntityInterface
{
    use EntityTrait;

    /**
     * @param string|int $identifier
     * @return void
     */
    public function __construct($identifier)
    {
        $this->setIdentifier($identifier);
    }
}
