<?php declare(strict_types=1);

namespace Averor\OAuthServerBundle\Controller;

use League\OAuth2\Server\AuthorizationServer;
use League\OAuth2\Server\Exception\OAuthServerException;
use League\OAuth2\Server\Grant\RefreshTokenGrant;
use Symfony\Bridge\PsrHttpMessage\Factory\DiactorosFactory;
use Symfony\Bridge\PsrHttpMessage\Factory\HttpFoundationFactory;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class OAuthTokenController
 *
 * @package Averor\OAuthServerBundle\Controller
 * @author Averor <averor.dev@gmail.com>
 */
class OAuthTokenController extends AbstractController
{
    /** @var AuthorizationServer */
    protected $server;

    /** @var RefreshTokenGrant */
    protected $refreshTokenGrant;

    /**
     * @param AuthorizationServer $server
     * @param RefreshTokenGrant $refreshTokenGrant
     */
    public function __construct(AuthorizationServer $server, RefreshTokenGrant $refreshTokenGrant)
    {
        $this->server = $server;
        $this->refreshTokenGrant = $refreshTokenGrant;
    }

    /**
     * @param Request $request
     * @return Response
     * @throws \Exception
     */
    public function __invoke(Request $request) : Response
    {
        try {
            $psr7Factory = new DiactorosFactory();
            $httpFoundationFactory = new HttpFoundationFactory();

            $psrRequest = $psr7Factory->createRequest($request);
            $psrResponse = $psr7Factory->createResponse(new Response());

            $this->server->respondToAccessTokenRequest(
                $psrRequest,
                $psrResponse
            );

            $r = $httpFoundationFactory->createResponse(
                $psrResponse
            );
            $r->headers->set('Content-Type', 'application/json');

            return $r;

        } catch (OAuthServerException $e) {

            $r = (new HttpFoundationFactory())->createResponse(
                $e->generateHttpResponse(
                    (new DiactorosFactory())->createResponse(
                        new Response()
                    )
                )
            );
            $r->headers->set('Content-Type', 'application/json');

            return $r;

        } catch (\Throwable $e) {

            return new JsonResponse([
                'error' => 'general',
                'message' => $e->getMessage()
            ], Response::HTTP_INTERNAL_SERVER_ERROR, []);
        }
    }
}
