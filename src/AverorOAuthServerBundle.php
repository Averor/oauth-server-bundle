<?php declare(strict_types=1);

namespace Averor\OAuthServerBundle;

use Averor\OAuthServerBundle\DependencyInjection\BundleExtension;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;


/**
 * Class AverorOAuthServerBundle
 *
 * @package Averor\OAuthServerBundle
 * @author Averor <averor.dev@gmail.com>
 */
class AverorOAuthServerBundle extends Bundle
{
//    public function build(ContainerBuilder $container)
//    {
//        parent::build($container);
//
//        $container->addCompilerPass(
//            ...
//        );
//    }

    public function getContainerExtension()
    {
        return new BundleExtension();
    }
}
