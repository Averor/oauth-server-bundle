<?php declare(strict_types=1);

namespace Averor\OAuthServerBundle\Repository;

use League\OAuth2\Server\Entities\AuthCodeEntityInterface;
use League\OAuth2\Server\Exception\UniqueTokenIdentifierConstraintViolationException;
use League\OAuth2\Server\Repositories\AuthCodeRepositoryInterface;

/**
 * Class AuthCodeRepository
 *
 * @package Averor\OAuthServerBundle\Repository
 * @author Averor <averor.dev@gmail.com>
 */
class AuthCodeRepository implements AuthCodeRepositoryInterface
{
    /** @var DatabaseFacade */
    protected $db;

    /** @var string */
    protected $collection;

    /**
     * @param DatabaseFacade $db
     * @param string $accessTokenRepoCollectionName
     */
    public function __construct(DatabaseFacade $db, string $accessTokenRepoCollectionName)
    {
        $this->db = $db;
        $this->collection = $accessTokenRepoCollectionName;
    }

    /**
     * Creates a new AuthCode
     *
     * @return AuthCodeEntityInterface
     */
    public function getNewAuthCode() : AuthCodeEntityInterface
    {
        // @todo Implement AuthCodeRepository::getNewAuthCode()

//        return new AuthCode();
    }

    /**
     * Persists a new auth code to permanent storage.
     *
     * @param AuthCodeEntityInterface $authCodeEntity
     * @return void
     * @throws UniqueTokenIdentifierConstraintViolationException
     */
    public function persistNewAuthCode(AuthCodeEntityInterface $authCodeEntity) : void
    {
        // @todo Implement AuthCodeRepository::persistNewAuthCode()

//        $id = $authCodeEntity->getIdentifier();
//
//        if ($this->dbExists(static::COLLECTION, $id)) {
//            throw UniqueTokenIdentifierConstraintViolationException::create();
//        }
//
//        $document = [
//            '_id' => $authCodeEntity->getIdentifier(),
//            'userId' => $authCodeEntity->getUserIdentifier(),
//            'clientId' => $authCodeEntity->getClient()->getIdentifier(),
//            'scopes' => $this->scopesToArray($authCodeEntity->getScopes()),
//            'revoked' => false,
//            'expiresAt' => $authCodeEntity->getExpiryDateTime(),
//        ];
//
//        $this->dbInsert(static::COLLECTION, $document);
    }

    /**
     * Revoke an auth code.
     *
     * @param string $codeId
     * @return void
     */
    public function revokeAuthCode($codeId) : void
    {
        // @todo Implement AuthCodeRepository::revokeAuthCode()

//        $this->dbUpdate(static::COLLECTION, ['_id' => $codeId, 'revoked' => true]);
    }

    /**
     * Check if the auth code has been revoked.
     *
     * @param string $codeId
     * @return bool Return true if this code has been revoked
     */
    public function isAuthCodeRevoked($codeId) : bool
    {
        // @todo Implement AuthCodeRepository::isAuthCodeRevoked()

//        $document = $this->dbGet(static::COLLECTION, $codeId);
//        return $document['revoked'] ?? true;
    }
}