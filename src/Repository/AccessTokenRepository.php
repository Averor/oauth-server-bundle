<?php declare(strict_types=1);

namespace Averor\OAuthServerBundle\Repository;

use Averor\OAuthServerBundle\Entity\AccessToken;
use League\OAuth2\Server\Entities\AccessTokenEntityInterface;
use League\OAuth2\Server\Entities\ClientEntityInterface;
use League\OAuth2\Server\Entities\ScopeEntityInterface;
use League\OAuth2\Server\Exception\UniqueTokenIdentifierConstraintViolationException;
use League\OAuth2\Server\Repositories\AccessTokenRepositoryInterface;

/**
 * Class AccessTokenRepository
 *
 * @package Averor\OAuthServerBundle\Repository
 * @author Averor <averor.dev@gmail.com>
 */
class AccessTokenRepository implements AccessTokenRepositoryInterface
{
    /** @var DatabaseFacade */
    protected $db;

    /** @var string */
    protected $collection;

    /**
     * @param DatabaseFacade $db
     * @param string $accessTokenRepoCollectionName
     */
    public function __construct(DatabaseFacade $db, string $accessTokenRepoCollectionName)
    {
        $this->db = $db;
        $this->collection = $accessTokenRepoCollectionName;
    }

    /**
     * Create a new access token
     *
     * @param ClientEntityInterface $clientEntity
     * @param ScopeEntityInterface[] $scopes
     * @param string $userIdentifier
     * @return AccessTokenEntityInterface
     */
    public function getNewToken(
        ClientEntityInterface $clientEntity,
        array $scopes,
        $userIdentifier = null
    ) : AccessTokenEntityInterface {

        return new AccessToken(
            $clientEntity,
            $userIdentifier,
            $scopes
        );
    }

    /**
     * Persists a new access token to permanent storage.
     *
     * @param AccessTokenEntityInterface $accessTokenEntity
     * @return void
     * @throws UniqueTokenIdentifierConstraintViolationException
     * @throws \Exception
     */
    public function persistNewAccessToken(AccessTokenEntityInterface $accessTokenEntity) : void
    {
        $id = $accessTokenEntity->getIdentifier();

        if ($this->db->exists($this->collection, $id)) {
            throw UniqueTokenIdentifierConstraintViolationException::create();
        }

        $document = [
            'id' => $id,
            'userId' => $accessTokenEntity->getUserIdentifier(),
            'clientId' => $accessTokenEntity->getClient()->getIdentifier(),
            'scopes' => $this->db->serializeScopes($accessTokenEntity->getScopes()),
            'revoked' => false,
            'createdAt' => new \DateTimeImmutable(),
            'updatedAt' => new \DateTimeImmutable(),
            'expiresAt' => $accessTokenEntity->getExpiryDateTime(),
        ];

        $this->db->insert($this->collection, $document);
    }

    /**
     * Revoke an access token.
     *
     * @param string $tokenId
     * @return void
     */
    public function revokeAccessToken($tokenId) : void
    {
        $this->db->update(
            $this->collection,
            $tokenId,
            ['revoked' => true]
        );
    }

    /**
     * Check if the access token has been revoked.
     *
     * @param string $tokenId
     * @return bool Return true if this token has been revoked
     */
    public function isAccessTokenRevoked($tokenId) : bool
    {
        $result = $this->db->get(
            $this->collection,
            $tokenId
        );

        return $result['revoked'] ?? true;
    }
}
