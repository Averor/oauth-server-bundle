<?php declare(strict_types=1);

namespace Averor\OAuthServerBundle\Repository;

use Averor\OAuthServerBundle\Entity\RefreshToken;
use League\OAuth2\Server\Entities\RefreshTokenEntityInterface;
use League\OAuth2\Server\Exception\UniqueTokenIdentifierConstraintViolationException;
use League\OAuth2\Server\Repositories\AccessTokenRepositoryInterface;
use League\OAuth2\Server\Repositories\RefreshTokenRepositoryInterface;

/**
 * Class RefreshTokenRepository
 *
 * @package Averor\OAuthServerBundle\Repository\Mongo
 * @author Averor <averor.dev@gmail.com>
 */
class RefreshTokenRepository implements RefreshTokenRepositoryInterface
{
    /** @var DatabaseFacade */
    protected $db;

    /** @var string */
    protected $collection;

    /** @var AccessTokenRepositoryInterface */
    protected $accessTokenRepository;

    /**
     * @param DatabaseFacade $db
     * @param string $accessTokenRepoCollectionName
     * @param AccessTokenRepositoryInterface $accessTokenRepository
     */
    public function __construct(
        DatabaseFacade $db,
        string $accessTokenRepoCollectionName,
        AccessTokenRepositoryInterface $accessTokenRepository
    ) {
        $this->db = $db;
        $this->collection = $accessTokenRepoCollectionName;
        $this->accessTokenRepository = $accessTokenRepository;
    }

    /**
     * Creates a new refresh token
     *
     * @return RefreshTokenEntityInterface
     */
    public function getNewRefreshToken() : RefreshTokenEntityInterface
    {
        return new RefreshToken();
    }

    /**
     * @param RefreshTokenEntityInterface $refreshTokenEntity
     * @return void
     * @throws UniqueTokenIdentifierConstraintViolationException
     * @throws \Exception
     */
    public function persistNewRefreshToken(RefreshTokenEntityInterface $refreshTokenEntity) : void
    {
        $id = $refreshTokenEntity->getIdentifier();

        if ($this->db->exists($this->collection, $id)) {
            throw UniqueTokenIdentifierConstraintViolationException::create();
        }

        $data = [
            'id' => $id,
            'accessTokenId' => $refreshTokenEntity->getAccessToken()->getIdentifier(),
            'revoked' => false,
            'expiresAt' => $refreshTokenEntity->getExpiryDateTime(),
        ];

        $this->db->insert(
            $this->collection,
            $data
        );
    }

    /**
     * Revoke the refresh token.
     *
     * @param string $tokenId
     * @return void
     */
    public function revokeRefreshToken($tokenId) : void
    {
        $this->db->update(
            $this->collection,
            $tokenId,
            ['revoked' => true]
        );
    }

    /**
     * Check if the refresh token has been revoked.
     *
     * @param string $tokenId
     * @return bool Return true if this token has been revoked
     */
    public function isRefreshTokenRevoked($tokenId) : bool
    {
        $data = $this->db->get(
            $this->collection,
            $tokenId
        );

        if (!$data || $data['revoked']) {
            return true;
        }

        return $this->accessTokenRepository->isAccessTokenRevoked(
            $data['accessTokenId']
        );
    }
}
