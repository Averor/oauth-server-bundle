<?php declare(strict_types=1);

namespace Averor\OAuthServerBundle\Repository;

/**
 * Interface DatabaseFacade
 *
 * @package Averor\OAuthServerBundle\Repository
 * @author Averor <averor.dev@gmail.com>
 */
interface DatabaseFacade
{
    public function exists(string $source, string $id);
    public function get(string $source, string $id);
    public function insert(string $target, array $data);
    public function update(string $target, string $id, array $data);

    public function serializeScopes(array $scopes) : string;
}
