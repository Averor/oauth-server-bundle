<?php declare(strict_types=1);

namespace Averor\OAuthServerBundle\Repository;

use Averor\OAuthServerBundle\Entity\Client;
use League\OAuth2\Server\Entities\ClientEntityInterface;
use League\OAuth2\Server\Repositories\ClientRepositoryInterface;

/**
 * Class ClientRepository
 *
 * @package Averor\OAuthServerBundle\Repository
 * @author Averor <averor.dev@gmail.com>
 */
class ClientRepository implements ClientRepositoryInterface
{
    /** @var DatabaseFacade */
    protected $db;

    /** @var string */
    protected $collection;

    /**
     * @param DatabaseFacade $db
     * @param string $accessTokenRepoCollectionName
     */
    public function __construct(DatabaseFacade $db, string $accessTokenRepoCollectionName)
    {
        $this->db = $db;
        $this->collection = $accessTokenRepoCollectionName;
    }

    /**
     * @param string $id
     * @param string $name
     * @param null|string $secret
     * @param string $redirect
     * @param bool|null $personalAccess
     * @param bool|null $password
     * @return void
     */
    public function save(
        string $id,
        string $name,
        ?string $secret,
        string $redirect,
        ?bool $personalAccess = false,
        ?bool $password = false
    ) : void {

        $data = [
            'id' => $id,
            'name' => $name,
            'secret' => $secret,
            'redirect' => $redirect,
            'personalAccess' => $personalAccess,
            'password' => $password,
            'revoked' => false
        ];

        $this->db->insert(
            $this->collection,
            $data
        );
    }

    /**
     * Get a client.
     *
     * @param string $clientIdentifier The client's identifier
     * @param null|string $grantType The grant type used (if sent)
     * @param null|string $clientSecret The client's secret (if sent)
     * @param bool $mustValidateSecret If true the client must attempt to validate the secret if the client
     *                                        is confidential
     * @return ClientEntityInterface|null
     */
    public function getClientEntity(
        $clientIdentifier,
        $grantType = null,
        $clientSecret = null,
        $mustValidateSecret = true
    ) : ?ClientEntityInterface {

        $data = $this->db->get(
            $this->collection,
            $clientIdentifier
        );

        if (!$data || $data['revoked']) {
            return null;
        }

        if (
            $mustValidateSecret
            && !hash_equals($data['secret'], (string) $clientSecret)
        ) {
            return null;
        }

        return new Client(
            $clientIdentifier,
            $data['name'],
            $data['redirect']
        );
    }
}
