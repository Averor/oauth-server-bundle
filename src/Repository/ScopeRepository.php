<?php declare(strict_types=1);

namespace Averor\OAuthServerBundle\Repository;

use Averor\OAuthServerBundle\Entity\Scope;
use League\OAuth2\Server\Entities\ClientEntityInterface;
use League\OAuth2\Server\Entities\ScopeEntityInterface;
use League\OAuth2\Server\Repositories\ScopeRepositoryInterface;

/**
 * Class ScopeRepository
 *
 * @package Averor\OAuthServerBundle\Repository\Mongo
 * @author Averor <averor.dev@gmail.com>
 *
 * @todo Db repo...
 */
class ScopeRepository implements ScopeRepositoryInterface
{
    /**
     * Return information about a scope.
     *
     * @param string $identifier The scope identifier
     * @return ScopeEntityInterface|null
     */
    public function getScopeEntityByIdentifier($identifier) : ?ScopeEntityInterface
    {
        if ($identifier === 'ALL') {
            return new Scope($identifier);
        }

        return null;
    }

    /**
     * Given a client, grant type and optional user identifier validate the set of scopes
     * requested are valid and optionally append additional scopes or remove requested scopes.
     *
     * @param ScopeEntityInterface[] $scopes
     * @param string $grantType
     * @param ClientEntityInterface $clientEntity
     * @param null|string $userIdentifier
     *
     * @return ScopeEntityInterface[]
     */
    public function finalizeScopes(
        array $scopes,
        $grantType,
        ClientEntityInterface $clientEntity,
        $userIdentifier = null
    ) {
        return [
            new Scope('ALL')
        ];
    }
}
