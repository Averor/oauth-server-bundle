<?php declare(strict_types=1);

namespace Averor\OAuthServerBundle\Repository\Mongo;

use Averor\OAuthServerBundle\Repository\DatabaseFacade;
use League\OAuth2\Server\Entities\ScopeEntityInterface;
use MongoDB\Database;
use MongoDB\Model\BSONDocument;

/**
 * Class MongoFacade
 *
 * @package Averor\OAuthServerBundle\Repository\Mongo
 * @author Averor <averor.dev@gmail.com>
 */
class MongoFacade implements DatabaseFacade
{
    /** @var Database */
    protected $mongo;

    public function __construct(Database $mongo)
    {
        $this->mongo = $mongo;
    }

    public function exists(string $source, string $id)
    {
        $collection = $this->mongo->selectCollection(
            $source
        );

        $document = $collection->findOne(
            ['_id' => $id],
            ['projection' => ["_id" => 1]]
        );

        return $document ? true : false;
    }

    public function get(string $source, string $id)
    {
        $collection = $this->mongo->selectCollection(
            $source
        );

        $document = static::bsonDocumentToArray(
            $collection->findOne(
                ['_id' => $id]
            )
        );

        if (!$document) {
            return null;
        }

        $document['id'] = $document['_id'];
        unset($document['_id']);

        return $document;
    }

    public function insert(string $target, array $data)
    {
        $collection = $this->mongo->selectCollection(
            $target
        );

        if (
            array_key_exists('id', $data)
            && !array_key_exists('_id', $data)
        ) {
            $data['_id'] = $data['id'];
        }

        unset($data['id']);

        $collection->insertOne($data);
    }

    public function update(string $target, string $id, array $data)
    {
        $collection = $this->mongo->selectCollection(
            $target
        );

        if (
            array_key_exists('id', $data)
            && !array_key_exists('_id', $data)
        ) {
            $data['_id'] = $data['id'];
        }

        unset($data['id']);

        $collection->updateOne(
            ['_id' => $id],
            ['$set' => $data]
        );
    }

    public function serializeScopes(array $scopes) : string
    {
        return json_encode(array_map(function (ScopeEntityInterface $scope) {
            return $scope->getIdentifier();
        }, $scopes));
    }

    /**
     * @param BSONDocument|array|null $document
     * @return array
     */
    protected static function bsonDocumentToArray($document) : ?array
    {
        if (!$document) {
            return null;
        }

        // do not use $document->getArrayCopy(), as children documents will not be casted to array
        // and currently there is no native toArray() method

        return json_decode(
            json_encode(
                $document,
                JSON_FORCE_OBJECT
            ),
            true
        );
    }
}
